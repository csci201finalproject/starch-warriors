package starchWarriorsServer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;


public class ServerGUI extends JFrame {
	private JButton button;
	private JTextArea text;
	private JScrollPane jsp;
	private int portnum;
	private Server stServer;
	
	public ServerGUI(){
		instantiateComponents();
		createGUI();
		addActions();
	}
	
	private void instantiateComponents(){
		button = new JButton(Constants.buttonStartName);
		text = new JTextArea();
		jsp = new JScrollPane(text);
		text.setEditable(false);
		
		portnum = 6789;
	}
	
	private void createGUI(){
		setSize(400, 400);
		setLocation(200, 200);
		setLayout(new BorderLayout());
		
		getContentPane().requestFocus();
		text.setText("");
		add(jsp);
				
		button.setFocusable(true);
		button.requestFocus();
		add(button, BorderLayout.SOUTH);
	}
	
	private void addActions(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				if(button.getText().equals(Constants.buttonStartName)){
					button.setText(Constants.buttonStopName);
					addMessage("Server started on port:" + portnum);
					stServer = new Server(ServerGUI.this, portnum);
//					teServer.listen();
				}
				else if(button.getText().equals(Constants.buttonStopName)){
					button.setText(Constants.buttonStartName);
					stServer.stopListening();
					addMessage("Server stopped");
				}
			}
		});
	}
	
	public void addMessage(String str){
		text.append(str + "\n");
	}
	
	public void addMessage(String str, int client){
		text.append(client + ": " + str + "\n");
	}
	
	public static void main(String[] args){
		try{
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			ServerGUI sg = new ServerGUI();
			sg.setVisible(true);
		} catch(Exception e){
			System.out.println("Warning! Cross-Platform L&F not used!");
			System.out.println(e.getMessage());
		}
	}
	
}
