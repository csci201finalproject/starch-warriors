package starchWarriorsServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class AttackServerThread extends Thread{
	private InputStreamReader isr1;
	private BufferedReader br1;
	private PrintWriter pw1;
	
	private InputStreamReader isr2;
	private BufferedReader br2;
	private PrintWriter pw2;
	
	private ServerGUI gui;
	private Server tes;
	
	public AttackServerThread(Socket s1, Socket s2, ServerGUI gui, Server tes){
		System.out.println("AttackServerThread");
		this.gui = gui;
		this.tes = tes;
		try {
			pw1 = new PrintWriter(s1.getOutputStream());
			isr1 = new InputStreamReader(s1.getInputStream());
			br1 = new BufferedReader(isr1);
			
			pw2 = new PrintWriter(s2.getOutputStream());
			isr2 = new InputStreamReader(s2.getInputStream());
			br2 = new BufferedReader(isr2);
			
			this.start();
		} catch (IOException ioe) {
			System.out.println("AttackServerThread() ioe: " + ioe.getMessage());
		}
	}
	
	public void run(){
		try{
			while(true){
				String client1Message = br1.readLine();
				String client2Message = br2.readLine();
				if(client1Message != null & client2Message != null){
					System.out.println("Received from client1: " + client1Message);
					System.out.println("Received from client2: " + client2Message);
					gui.addMessage(client1Message, 1);
					gui.addMessage(client2Message, 2);
					respond(client1Message, 1);
					respond(client2Message, 2);
				}
			}
		} catch (IOException ioe) {
			System.out.println("Client Disconnected");
		} finally {
			tes.removeAttackServerThread(this);
		}
	}
	
	public void respond(String msg, int client) throws IOException{
		//1 is fire
		//2 is water
		//3 is leaf
		if(msg.equals("1")){
			if(client == 1){
				pw2.println(msg);
			}
		}
		if(msg.equals("2")){
			if(client == 1){
				pw2.println(msg);
			}
		}
		if(msg.equals("3")){
			if(client == 1){
				pw2.println(msg);
			}
		}
		if(msg.equals("1")){
			if(client == 2){
				pw1.println(msg);
			}
		}
		if(msg.equals("2")){
			if(client == 2){
				pw1.println(msg);
			}
		}
		if(msg.equals("3")){
			if(client == 2){
				pw1.println(msg);
			}
		}
		pw1.flush();
		pw2.flush();
	}
}
