package starchWarriorsServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import com.mysql.jdbc.Driver;

public class SQLDriver {
	private Connection con;
	
	public SQLDriver() {
		try {
			new Driver();
		} catch(SQLException sqle){
			System.out.println("sqle SQLDriver(); " + sqle.getMessage());
		}
	}
	
	public void connect(){
		try{
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/SWUserInfo?user=root&password=1234567890");
		}
		catch(SQLException sqle){
			System.out.println("sqle connect(): " + sqle.getMessage());
		}
	}
	
	public void stop(){
		try {
			con.close();
		} catch(SQLException sqle){
			System.out.println("sqle stop(): " + sqle.getMessage());
		}
	}
	
	private final static String loginQuery = "SELECT USERNAME FROM Users WHERE username=? AND password=?;";
	public boolean checkLogin(String username, String password){
		try {
			PreparedStatement ps = con.prepareStatement(loginQuery);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				//System.out.println(result.getString(1));
				return true;
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	private final static String updateTimeQuery = "UPDATE users SET timest=? WHERE username=?;";
	public void updateTime(String timestamp, String username){
		try {
			PreparedStatement ps = con.prepareStatement(updateTimeQuery);
			ps.setString(1, timestamp);
			ps.setString(2, username);
			int result = ps.executeUpdate();
			if(result>0){
				System.out.println("Update time success");
			} else{
				System.out.println("Update time fail");
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	private final static String insertQuery = "INSERT INTO Users (username, password, timest) "
			+ "VALUES (?, ?, ?);";
	public boolean checkSignup(String username, String password, String timestamp){
		try {
			PreparedStatement ps = con.prepareStatement(insertQuery);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, timestamp);
			int result = ps.executeUpdate();
			if(result>0){
				System.out.println("Signup success");
				return true;
			} else{
				System.out.println("Signup failure");
				return false;
			}
		} catch (SQLException sqle){
			System.out.println("sqle: " + sqle.getMessage());
		}
		return false;
	}
}
