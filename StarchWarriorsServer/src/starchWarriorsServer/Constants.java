package starchWarriorsServer;

public class Constants {
	public static final String buttonStartName = "Start";
	public static final String buttonStopName = "Stop";
	
	
	public static final String databaseProtocolString = "Database";
	public static final String loginProtocolString = "Login";
	public static final String signupProtocolString = "Signup";
	
	public static final String battleProtocolString = "Battle";
	public static final String clientInformationString = "Client Information";
	
	public static final String attackProtocolString = "Attack";	
}
