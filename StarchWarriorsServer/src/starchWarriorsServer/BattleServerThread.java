package starchWarriorsServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class BattleServerThread extends Thread{
	
	private InputStreamReader isr1;
	private BufferedReader br1;
	private PrintWriter pw1;
	
	private InputStreamReader isr2;
	private BufferedReader br2;
	private PrintWriter pw2;
	
	private ServerGUI gui;
	private Server tes;
	
	public BattleServerThread(Socket s1, Socket s2, ServerGUI gui, Server tes){
		System.out.println("BattleServerThread");
		this.gui = gui;
		this.tes = tes;
		try {
			pw1 = new PrintWriter(s1.getOutputStream());
			isr1 = new InputStreamReader(s1.getInputStream());
			br1 = new BufferedReader(isr1);
			
			pw2 = new PrintWriter(s2.getOutputStream());
			isr2 = new InputStreamReader(s2.getInputStream());
			br2 = new BufferedReader(isr2);
			
			pw1.println("Connected");
			pw2.println("Connected");
			
			pw1.flush();
			pw2.flush();
			
			this.start();
		} catch (IOException ioe) {
			System.out.println("BattleServerThread() ioe: " + ioe.getMessage());
		}
	}
	
	@Override
	public void run(){
		try{
			while(true){
				String client1Message = br1.readLine();
				String client2Message = br2.readLine();
				if(client1Message != null & client2Message != null){
					System.out.println("Received from client1: " + client1Message);
					System.out.println("Received from client2: " + client2Message);
					gui.addMessage(client1Message, 1);
					gui.addMessage(client2Message, 2);
					respond(client1Message, 1);
					respond(client2Message, 2);
				}
			}
		} catch(IOException ioe) {
			System.out.println("Client Disconnected");
		} finally {
			tes.removeBattleServerThread(this);
		}
	}
	
	public void respond(String msg, int client) throws IOException{
		if(msg.length() > Constants.clientInformationString.length()){
			if(msg.substring(0, Constants.clientInformationString.length()).equals(Constants.clientInformationString)){
				initializeBattleScreen(msg, client);
			}
		}
	}

	private void initializeBattleScreen(String msg, int client) {
		System.out.println("Connected");
		String[] infos = msg.split(";")[1].split(" ");
		String pname = infos[0].split(":")[1];
		String plvl = infos[1].split(":")[1];
		String pbattlehealth = infos[2].split(":")[1];
		String pfirelvl = infos[3].split(":")[1];
		String pwaterlvl = infos[4].split(":")[1];
		String pgrasslvl = infos[5].split(":")[1];
		
		if(client == 1){
			pw2.println(pname);
			pw2.println(pbattlehealth);
			pw2.println(plvl);
			pw2.println(pfirelvl);
			pw2.println(pwaterlvl);
			pw2.println(pgrasslvl);
			pw2.flush();
		} else if(client == 2){
			pw1.println(pname);
			pw1.println(pbattlehealth);
			pw1.println(plvl);
			pw1.println(pfirelvl);
			pw1.println(pwaterlvl);
			pw1.println(pgrasslvl);
			pw1.flush();
		}
		
	}
}
