package starchWarriorsServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Vector;

public class Server extends Thread {
	ServerGUI sts;
	Vector<ServerThread> serverThreads;
	Vector<BattleServerThread> bServerThreads;
	Vector<AttackServerThread> aServerThreads;
	ServerSocket ss;
	Socket s;
	int portNum;
	LinkedList<Socket> qBattle; 
	LinkedList<Socket> qAttack;
	
	public Server(ServerGUI gui, int port){
		sts = gui;
		portNum = port;
		ss = null;
		serverThreads = new Vector<ServerThread>();
		bServerThreads = new Vector<BattleServerThread>();
		aServerThreads = new Vector<AttackServerThread>();
		qBattle = new LinkedList<Socket>();
		qAttack = new LinkedList<Socket>();
		try {
			ss = new ServerSocket(portNum);
			ss.setReuseAddress(true);
		} catch (IOException ioe) {
			System.out.println("ioe TextEditorServer(): " + ioe.getMessage());
		}
		this.start();
	}
	
	public void stopListening(){
		try {
			ss.close();
		} catch (IOException ioe) {
			System.out.println("ioe TextEditorServer stopListening(): " + ioe.getMessage());
		} //catch (InterruptedException ie) {
		//	System.out.println("TextEditorServer() stopListening() ie: " + ie.getMessage());
		//} 
	}
	
	@Override
	public void run() {
		try {
			while(true){
				//syncObject.wait();
				System.out.println("Waiting for connection");
				s = ss.accept();
				//Will print out if connection successful
				System.out.println("Connection successful");
				InputStreamReader isr = new InputStreamReader(s.getInputStream());
				BufferedReader br = new BufferedReader(isr);
				String msg = br.readLine();
				System.out.println(msg);
				//br.close();
				//isr.close();
				if(msg.equals(Constants.databaseProtocolString)){
					ServerThread st = new ServerThread(s, sts, this);
					serverThreads.add(st);
					System.out.println("Added Server Thread");
				}
				else if(msg.equals(Constants.battleProtocolString)){
					qBattle.add(s);
					if(qBattle.size() >= 2){
						BattleServerThread bst = new BattleServerThread(qBattle.pop(), qBattle.pop(), sts, this);
						bServerThreads.addElement(bst);
					}
				}
				else if(msg.equals(Constants.attackProtocolString)){
					if(qAttack.isEmpty()){
						qAttack.add(s);
					}
					else if(!qAttack.element().getRemoteSocketAddress().toString().equals(s.getRemoteSocketAddress().toString())){
						qAttack.add(s);
					}
					if(qAttack.size() >= 2){
						AttackServerThread ast = new AttackServerThread(qAttack.pop(),qAttack.pop(),sts, this); 
						aServerThreads.addElement(ast);
					}
				}
			}
		} catch (IOException ioe) {
				System.out.println("TextEditorServer Run() ioe: " + ioe.getMessage());
		//} catch (InterruptedException ie) {
			//System.out.println("TextEditorServer() run() ie: " + ie.getMessage());
		} finally {
			try {
				if(ss != null){
					ss.close();
				}
				if(s != null){
					s.close();
				}
				serverThreads.removeAllElements();
				bServerThreads.removeAllElements();
			} catch (IOException ioe){
				System.out.println("TextEditorServer() ioe closing ss: " + ioe.getMessage());
			}
		}
	}
	
	
	public void removeServerThread(ServerThread thread){
		serverThreads.remove(thread);
	}
	public void removeBattleServerThread(BattleServerThread thread){
		bServerThreads.remove(thread);
	}
	public void removeAttackServerThread(AttackServerThread thread){
		aServerThreads.remove(thread);
	}
	
}
