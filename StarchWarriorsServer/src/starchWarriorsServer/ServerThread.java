package starchWarriorsServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Vector;

public class ServerThread extends Thread{
	//private ObjectOutputStream oos;
	//private ObjectInputStream ois;
	
	private InputStreamReader isr;
	private BufferedReader br;
	private PrintWriter pw;
	
	private ServerGUI gui;
	private Server tes;
	
	private SQLDriver driver;
	
	public ServerThread(Socket s, ServerGUI gui, Server tes){
		System.out.println("ServerThread");
		this.gui = gui;
		this.tes = tes;
		driver = new SQLDriver();
		try {
			pw = new PrintWriter(s.getOutputStream());
			//ois = new ObjectInputStream(s.getInputStream());

			isr = new InputStreamReader(s.getInputStream());
			br = new BufferedReader(isr);
			//pw = new PrintWriter(s.getOutputStream());

			this.start();
		} catch (IOException ioe) {
			System.out.println("ServerThread() ioe: " + ioe.getMessage());
		}
	}

	@Override
	public void run() {
		try{
			while(true){
				//String clientMessage = (String) ois.readObject();
				String clientMessage = br.readLine();
				if(clientMessage != null){
					System.out.println("Received from client: " + clientMessage);
					gui.addMessage(clientMessage);
					respond(clientMessage);
				}
			}
		//} catch(ClassNotFoundException cnfe) {
		//	System.out.println("Class not found: " + cnfe.getMessage());
		} catch(IOException ioe) {
			System.out.println("Client Disconnected");
		//} catch (ClassNotFoundException cnfe) {
		//	System.out.println("ServerThread run() cnfe: " + cnfe.getMessage());
		} finally {
			tes.removeServerThread(this);
		}
	}
	
	public void respond(String msg) throws IOException{
		if(msg.length() > Constants.loginProtocolString.length()){
			if(msg.substring(0, Constants.loginProtocolString.length()).equals(Constants.loginProtocolString)){
				loginAttempt(msg);
			}
		}
		if(msg.length() > Constants.signupProtocolString.length()){
			if(msg.substring(0,Constants.signupProtocolString.length()).equals(Constants.signupProtocolString)){
				signupAttempt(msg);
			}
		}
		

//		if(msg.length() == Constants.connectionTestString.length()){
//			if(msg.equals(Constants.connectionTestString)){
//				oos.writeObject(Constants.connectionTestResponseString);
//				oos.flush();
//			}
//		}
	}

	private void loginAttempt(String msg) throws IOException {
		msg.substring(Constants.loginProtocolString.length(), msg.length());
		
		String[] strs = msg.substring(Constants.loginProtocolString.length(),msg.length()).split(" ");
		String username = strs[0].split(":")[1];
		String password = strs[1].split(":")[1];
		String timestamp = strs[2].split(":")[1] + " " + strs[3];

		driver.connect();
		if(driver.checkLogin(username, password)){
			String resp = "Login successful";
			gui.addMessage("Log-in success User:" + username);
			driver.updateTime(timestamp, username);
			pw.println(resp);
		}
		else{
			String resp = "Login failed";
			gui.addMessage("Log-in failure User:" + username);
			pw.println(resp);
		}
		pw.flush();
	}

	private void signupAttempt(String msg) throws IOException {
		msg.substring(Constants.signupProtocolString.length(), msg.length());
		
		String[] strs = msg.substring(Constants.signupProtocolString.length(),msg.length()).split(" ");
		String username = strs[0].split(":")[1];
		String password = strs[1].split(":")[1];
		String timestamp = strs[2].split(":")[1] + " " + strs[3];

		driver.connect();
		if(driver.checkSignup(username, password, timestamp)){
			String resp = "Signup successful";
			gui.addMessage("Sign-up success User:" + username);
			pw.println(resp);
		}
		else{
			String resp = "Signup failed";
			gui.addMessage("Sign-up failure User:" + username);
			pw.println(resp);
		}
		pw.flush();
	}
}

