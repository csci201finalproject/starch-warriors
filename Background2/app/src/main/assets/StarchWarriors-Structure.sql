DROP DATABASE if exists SWUserInfo;
CREATE DATABASE SWUserInfo;
USE SWUserInfo;


CREATE TABLE Users (
	username VARCHAR(50) PRIMARY KEY NOT NULL,
    password VARCHAR(50) NOT NULL,
    timest TIMESTAMP(6) NOT NULL
);

CREATE TABLE Pets (
	petID INT(10) PRIMARY KEY NOT NULL,
	username VARCHAR(50) NOT NULL,
	petname VARCHAR(50) NOT NULL,
    happiness FLOAT(1) NOT NULL,
    hungriness FLOAT(1) NOT NULL,
    cleanliness FLOAT(1) NOT NULL,
    fun FLOAT(1) NOT NULL,
    level INT(10) NOT NULL,
    experience INT(10) NOT NULL,
    money INT(10) NOT NULL,
    FOREIGN KEY fk1(username) REFERENCES Users(username)
);

CREATE TABLE Moves (
	movesetID INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    petID INT(10) NOT NULL,
    firename VARCHAR(50) NOT NULL,
    firelevel INT(10) NOT NULL,
    watername VARCHAR(50) NOT NULL,
    waterlevel INT(10) NOT NULL,
    grassname VARCHAR(50) NOT NULL,
    grasslevel INT(10) NOT NULL,
    FOREIGN KEY fk2(petID) REFERENCES Pets(petID)
);

INSERT INTO Users (username, password, timest)
	VALUES ('Sheldon', 'Cooper', '2016-04-22');
    
