package com.example.kayla.background2;

/**
 * Created by jimmychen on 4/15/16.
 */
public class Stat {
    float value = 0.5f;
    float max = 1.0f;
    float min = 0.0f;

    public void setDefault(){
        value = 0.5f;
    }

    public void increaseStat(float inc){
        if(value + inc > max){
            value = max;
        }
        else if(value + inc < min){
            value = min;
        }
        else{
            value += inc;
        }
    }

    public float getValue(){ return value;}
}
