package com.example.kayla.background2;

import android.widget.ImageView;

/**
 * Created by jimmychen on 4/12/16.
 */
public class Food extends GameObject{

    public Food(String name, ImageView image){
        super(name, image);
    }

    static void feed(Pet pet, float statChange){
        pet.feed(statChange);
    }
}
