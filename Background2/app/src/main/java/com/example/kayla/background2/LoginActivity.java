package com.example.kayla.background2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends FragmentActivity {

    public static final String USERNAME_MESSAGE = "LoginActivity.USERNAME_MESSAGE";

    private LoginFragment firstFragment;

    MediaPlayer loginsong;

    public static String IPaddress = "10.120.124.196";

    @Override
    protected void onNewIntent(Intent intent) {
        EditText passText = (EditText) findViewById(R.id.password_editText);
        passText.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        if (findViewById(R.id.login_layout) != null){

            if (savedInstanceState != null) {
                return;
            }

            firstFragment = new LoginFragment();

            getSupportFragmentManager().beginTransaction().add(R.id.login_layout, firstFragment).commit();
        }
        loginsong = MediaPlayer.create(this, R.raw.login);
        loginsong.setLooping(true);
        loginsong.start();
    }

    @Override
    protected void onPause(){
        super.onPause();
        if (loginsong != null) loginsong.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (loginsong != null) loginsong.start();

    }

    @Override
    public void onBackPressed(){
        return;
    }

    public void login(View view){
        //get username in string:
        EditText userText = (EditText) findViewById(R.id.username_editText);
        final String username = userText.getText().toString();

        //get password in string:
        EditText passText = (EditText) findViewById(R.id.password_editText);
        final String password = passText.getText().toString();

        if(username.isEmpty() | password.isEmpty()) {
            showAlertDialog("Error","Empty text field(s)", android.R.drawable.ic_dialog_alert);
            return;
        }

        if(IPaddress.isEmpty()){
            showAlertDialog("Error","IP address not set", android.R.drawable.ic_dialog_alert);
            return;
        }

        final String ip = IPaddress;

        Thread client = new Thread(){
            public void run(){
                try {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    Socket s = null;
                    try {
                        s = new Socket(ip, 6789);
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                .getOutputStream())), true);
                        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                        // WHERE YOU ISSUE THE COMMANDS
                        Date now = Calendar.getInstance().getTime();
                        Timestamp timest = new Timestamp(now.getTime());
                        out.println("Database");
                        out.flush();
                        Log.d("ClientActivity", "C: Sending command.");
                        out.println("Login:" + username + " password:" + password + " time:" + timest);
                        out.flush();
                        Log.d("ClientActivity", "C: Sent Login attempt");
                        String response = in.readLine();
                        if(response != null & response.equals("Login successful")){
                            intent.putExtra(USERNAME_MESSAGE, username);
                            startActivity(intent);
                        } else {
                            showAlertDialog("Login","Login failed. Wrong username or password.",android.R.drawable.ic_dialog_alert);
                        }
                    } catch (Exception e) {
                        showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                        Log.e("ClientActivity", "S: Error", e);
                    }
                    if(s != null)
                        s.close();
                } catch (Exception e) {
                    Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                }
            }
        };
        client.start();
        finish();
    }

    public void gotosignup(View view){
        if(IPaddress.isEmpty()){
            showAlertDialog("Error","Set valid IP Address first!", android.R.drawable.ic_dialog_alert);
            return;
        }

        SignupFragment newFragment = new SignupFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);

        transaction.replace(R.id.login_layout, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();

    }

    public void backtologin(View view){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

        transaction.replace(R.id.login_layout, firstFragment);
        transaction.addToBackStack(null);

        transaction.commit();

    }


    public boolean register(View view) {

        //get desired username from user entry:
        EditText newUserText = (EditText) findViewById(R.id.username_editText);
        final String newUsername = newUserText.getText().toString();

        //get desired password from entry:
        EditText newPassText = (EditText) findViewById(R.id.password_editText);
        final String newPassword = newPassText.getText().toString();

        EditText repeatPassText = (EditText) findViewById(R.id.repeat_editText);
        String repeatPassword = repeatPassText.getText().toString();
        if(! repeatPassword.equals(newPassword)) {
            showAlertDialog("Error", "Passwords do not match!", android.R.drawable.ic_dialog_alert);
            return false;
        }
        if( newUsername.isEmpty() | newPassword.isEmpty() | repeatPassword.isEmpty()){
            showAlertDialog("Error","Empty text field(s)", android.R.drawable.ic_dialog_alert);
            return false;
        }

        Thread client = new Thread(){
            public void run(){
                Socket s = null;
                try {
                    Log.v("ClientActivity:", "connecting to server");
                    s = new Socket(IPaddress, 6789);

                    try {
                        Log.d("ClientActivity", "C: Sending command.");
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                .getOutputStream())), true);
                        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                        // WHERE YOU ISSUE THE COMMANDS
                        out.println("Database");
                        out.flush();
                        Date now = Calendar.getInstance().getTime();
                        Timestamp timest = new Timestamp(now.getTime());
                        out.println("Signup:" + newUsername + " password:" + newPassword + " time:" + timest);
                        out.flush();
                        Log.d("ClientActivity", "C: Sent signup attempt");
                        String response = in.readLine();
                        if(response != null & response.equals("Signup successful")){
                            Log.d("ClientActivity", "C: Signup successful");
                            gotoNamepet();
                        } else {
                            showAlertDialog("Error","Signup unsuccessful", android.R.drawable.ic_dialog_alert);
                            Log.d("ClientActivity", "C: Signup unsuccessful");
                        }
                    } catch (Exception e) {
                        showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                        Log.e("ClientActivity", "S: Error", e);
                    }
                if(s !=null )
                    s.close();
                } catch (Exception e) {
                    Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                }
            }
        };
        client.start();

        return false;
    }


    public void gotoNamepet(){

        NamePetFragment newFragment = new NamePetFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);

        transaction.replace(R.id.login_layout, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    public void guestUser(View view){
        Intent intent = new Intent(this, MainActivity.class);
        EditText userText = (EditText) findViewById(R.id.username_editText);
        String message = userText.getText().toString();
        intent.putExtra(USERNAME_MESSAGE,message);
        String name = "Bobato";
        intent.putExtra("name",name);
        startActivity(intent);
        this.onStop();
    }

    private static final Pattern IP_ADDRESS
            = Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))");

    public void setIPAddress(View view){
        EditText ipText = (EditText) findViewById(R.id.connect_editText);
        String ip = ipText.getText().toString();
        Matcher matcher = IP_ADDRESS.matcher(ip);
        if (matcher.matches()) {
            IPaddress = ip;
            showAlertDialog("Success","IP address successfully set", android.R.drawable.ic_dialog_info);
        } else {
            showAlertDialog("Error","Not a valid IP address", android.R.drawable.ic_dialog_alert);
        }
    }

    public void showAlertDialog(String title, String message, int drawable_icon) {
        final String alert_title = title;
        final String alert_message = message;
        final int alert_icon = drawable_icon;
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(alert_title)
                        .setMessage(alert_message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(alert_icon)
                        .show();
            }
        });

    }

    public void namePet(View view){
        EditText editText = (EditText) findViewById(R.id.namePet_petName_editText);
        String name = editText.getText().toString();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("name",name);
        startActivity(intent);
    }
}
