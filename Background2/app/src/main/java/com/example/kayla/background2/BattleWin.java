package com.example.kayla.background2;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Bobo on 4/22/2016.
 */
public class BattleWin extends DialogFragment {
    private AnimationDrawable playerPotatoAnimation;
    private ImageView playerPotatoImage;
    private Button backButton;
    MediaPlayer winsong;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState){

        View rootView = inflater.inflate(R.layout.battle_win, container, false);
        backButton = (Button) rootView.findViewById(R.id.returnButton);
        playerPotatoImage = (ImageView) rootView.findViewById(R.id.happypotato);
        playerPotatoImage.setBackgroundResource(R.drawable.happy_potato_animation);
        //adjusts the size of the potato
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) playerPotatoImage.getLayoutParams();
        params.width = 200;
        params.height = 200;
        playerPotatoAnimation = (AnimationDrawable) playerPotatoImage.getBackground();

        playerPotatoImage.setLayoutParams(params);

        playerPotatoImage.post(new Runnable() {
            @Override
            public void run() {
                playerPotatoAnimation.start();
            }
        });
        addListenerOnButton();

        winsong = MediaPlayer.create(getContext(), R.raw.battle_win);
        winsong.start();

        return rootView;
    }

    private void addListenerOnButton() {
        backButton.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                intent.putExtra("screen", "win");
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                winsong.stop();
                getActivity().finish();
            }
        });
    }
}
