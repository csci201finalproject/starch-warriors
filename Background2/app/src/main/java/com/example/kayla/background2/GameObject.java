package com.example.kayla.background2;

import android.widget.ImageView;
/**
 * Created by Bobo on 4/12/2016.
 */
public class GameObject {
    private String name;
    private ImageView gameObjectImage;
    private boolean purchased;
    private boolean isSelected;

    public GameObject(String name, ImageView image) {
        this.name = name;
        gameObjectImage = image;
        purchased = false;
        isSelected = false;
    }

    public void buyItem() {
        purchased = true;
    }

    public void select() {
        isSelected = true;
    }

    public void unSelect() {
        isSelected = false;
    }

    public void setImage(ImageView image) { gameObjectImage = image;}
}
