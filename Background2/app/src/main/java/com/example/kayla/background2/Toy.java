package com.example.kayla.background2;

import android.widget.ImageView;

/**
 * Created by jimmychen on 4/12/16.
 */
public class Toy extends GameObject{

    public Toy(String name, ImageView image){
        super(name, image);
    }

    static void play(Pet pet, float statChange){
        pet.play(statChange);
    }
}
