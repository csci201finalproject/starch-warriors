package com.example.kayla.background2;

import android.widget.ImageView;

/**
 * Created by jimmychen on 4/12/16.
 */
public class CleaningTool extends GameObject{

    public CleaningTool(String name, ImageView image){
        super(name, image);
    }

    static void clean(Pet pet, float statChange){
        pet.clean(statChange);
    }
}
