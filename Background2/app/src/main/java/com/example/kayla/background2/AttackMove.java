package com.example.kayla.background2;

/**
 * Created by Anek on 4/12/2016.
 */
public class AttackMove {

    private int level;
    private String attackName;
    private int element;
    private Pet owner;

    public AttackMove(Pet pet, int element){
        this.element = element;
        this.level = 1;
    }

    public AttackMove(Pet pet, int element, int level){
        this.element = element;
        this.level = level;
    }
/*
    public int calculatePower(){
        int power = (owner.getLevel()-1)*2 + (level-1)*5 + baseAttack;
        return power;
    }*/

    public void inflictDamage(Pet enemy){

    }

    public void levelUp() {
        level++;
    }

    public String getName(){
        return attackName;
    }

    public void setName(String name) { attackName = name;}

    public int getLevel(){
        return level;
    }


}
