package com.example.kayla.background2;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StatisticsActivity extends AppCompatActivity {

    private ImageView redFiller;
    private ImageView greenFiller;
    private ImageView blueFiller;
    private ImageView yellowFiller;
    private boolean mute;
    private final float fullBar = 900.0f;

    private TextView petName;
    private TextView petLevel;
    private TextView petExp;
    Button back;

    MediaPlayer statssong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_statistics);
        addListenerOnButton();

        mute = getIntent().getBooleanExtra("mute", false);

        redFiller = (ImageView)findViewById(R.id.redFiller);
        greenFiller = (ImageView)findViewById(R.id.greenFiller);
        blueFiller = (ImageView)findViewById(R.id.blueFiller);
        yellowFiller = (ImageView)findViewById(R.id.yellowFiller);

        petName = (TextView)findViewById(R.id.nameText);
        petLevel = (TextView)findViewById(R.id.levelText);
        petExp = (TextView)findViewById(R.id.expText);

        String name = getIntent().getStringExtra("name");
        setPetName(name);

        statssong = MediaPlayer.create(this, R.raw.stats);
        if(!mute) statssong.start();

    }

    @Override
    protected void onPause(){
        super.onPause();
        statssong.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateStats();
        if(!mute) statssong.start();
    }


    private void updateStats(){
        Bundle extras = getIntent().getExtras();
        float health = extras.getFloat("health");
        float hungriness = extras.getFloat("hungriness");
        float fun = extras.getFloat("fun");
        float cleanliness = extras.getFloat("cleanliness");
        int level = extras.getInt("level");
        int expNeeded = extras.getInt("expNeeded");

        setHealth(health);
        setHunger(hungriness);
        setFun(fun);
        setClean(cleanliness);
        setPetLevel(level);
        setPetExp(expNeeded);
    }

    public void setHealth(float health){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) redFiller.getLayoutParams();
        params.width = (int)(fullBar * health);
        redFiller.setLayoutParams(params);
    }

    public void setHunger(float hunger){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) greenFiller.getLayoutParams();
        params.width = (int)(fullBar * hunger);
        greenFiller.setLayoutParams(params);
    }

    public void setClean(float clean){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) blueFiller.getLayoutParams();
        params.width = (int)(fullBar * clean);
        blueFiller.setLayoutParams(params);
    }

    public void setFun(float fun){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) yellowFiller.getLayoutParams();
        params.width = (int)(fullBar * fun);
        yellowFiller.setLayoutParams(params);
    }

    public void setPetName(String name){
        petName.setText(name);
    }

    public void setPetLevel(int level){
        petLevel.setText("Level: "+ level);
    }

    public void setPetExp(int exp){
        petExp.setText("Exp needed: " + exp);
    }

    public void addListenerOnButton() {

        back = (Button) findViewById(R.id.statbackbutton);

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("screen", "stats");
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                finish();

            }

        });

    }

}
