package com.example.kayla.background2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

public class ShopActivity extends Activity {

    Button back;
    TextView money;
    private boolean mute;
    int cash;
    int food=0, toy=0, tool=0;
    ImageButton food0, food1, food2, food3;
    //booleans for if purchased
    boolean[] f = new boolean[4];
    boolean[] t = new boolean[4];
    boolean[] c = new boolean[4];
    ImageButton toy0, toy1, toy2, toy3;
    ImageButton tool0, tool1, tool2, tool3;

    MediaPlayer shopsong;
    MediaPlayer chaching;
    MediaPlayer select;

    @Override
    protected void onNewIntent(Intent intent) {
        cash = intent.getIntExtra("cash", 1000);
        mute = intent.getBooleanExtra("mute", false);
        if(!mute) shopsong.start();
        String amount = Integer.toString(cash);
        money = (TextView) findViewById(R.id.wallet);
        money.setText(" " + amount);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.shopscreen);
        cash = getIntent().getIntExtra("cash", 0);
        mute = getIntent().getBooleanExtra("mute", false);
        f[0] = true; t[0] = true; c[0] = true;
        String amount = Integer.toString(cash);
        money = (TextView) findViewById(R.id.wallet);
        money.setText(" " + amount);
        addListenerOnImageButton();
        addListenerOnButton();
        shopsong = MediaPlayer.create(this, R.raw.shop);
        shopsong.setLooping(true);
        chaching = MediaPlayer.create(this, R.raw.chaching);
        select = MediaPlayer.create(this, R.raw.select);
    }

    @Override
    protected void onPause(){
        super.onPause();
        shopsong.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(!mute)
            shopsong.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
    }

    public void addListenerOnButton() {
        //final Context context = this;

        back = (Button) findViewById(R.id.shop2);

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("shopCash", cash);
                intent.putExtra("food", food);
                intent.putExtra("toy", toy);
                intent.putExtra("tool", tool);
                intent.putExtra("screen", "shop");
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

            }

        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("shopCash", cash);
        intent.putExtra("food", food);
        intent.putExtra("toy", toy);
        intent.putExtra("tool", tool);
        intent.putExtra("screen", "shop");
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }


    public void addListenerOnImageButton() {

        final Context context = this;

        food0 = (ImageButton) findViewById(R.id.food0);
        food0.setBackgroundColor(Color.BLACK);
        food1 = (ImageButton) findViewById(R.id.food1);
        food2 = (ImageButton) findViewById(R.id.food2);
        food3 = (ImageButton) findViewById(R.id.food3);

        toy0 = (ImageButton) findViewById(R.id.toy0);
        toy0.setBackgroundColor(Color.BLACK);
        toy1 = (ImageButton) findViewById(R.id.toy1);
        toy2 = (ImageButton) findViewById(R.id.toy2);
        toy3 = (ImageButton) findViewById(R.id.toy3);

        tool0 = (ImageButton) findViewById(R.id.tool0);
        tool0.setBackgroundColor(Color.BLACK);
        tool1 = (ImageButton) findViewById(R.id.tool1);
        tool2 = (ImageButton) findViewById(R.id.tool2);
        tool3 = (ImageButton) findViewById(R.id.tool3);

       /* food0.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!f0){
                    food0.setImageResource(R.drawable.cookie2);
                    f0 = true;
                }
            }

        }); */
        food0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                food = 0;
                food1.setBackgroundColor(Color.TRANSPARENT);
                food2.setBackgroundColor(Color.TRANSPARENT);
                food3.setBackgroundColor(Color.TRANSPARENT);
                food0.setBackgroundColor(Color.BLACK);
                if(!mute)
                select.start();
                return true;
            }
        });

        food1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!f[1]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        food1.setImageResource(R.drawable.bpizza);
                        f[1] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        food1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!f[1]) return true;
                food = 1;
                food0.setBackgroundColor(Color.TRANSPARENT);
                food2.setBackgroundColor(Color.TRANSPARENT);
                food3.setBackgroundColor(Color.TRANSPARENT);
                food1.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });

        food2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!f[2]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        food2.setImageResource(R.drawable.bhotdog);
                        f[2] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        food2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!f[2]) return true;
                food=2;
                food1.setBackgroundColor(Color.TRANSPARENT);
                food0.setBackgroundColor(Color.TRANSPARENT);
                food3.setBackgroundColor(Color.TRANSPARENT);
                food2.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });

        food3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!f[3]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        food3.setImageResource(R.drawable.bslushie);
                        f[3] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        food3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!f[3]) return true;
                food=3;
                food1.setBackgroundColor(Color.TRANSPARENT);
                food2.setBackgroundColor(Color.TRANSPARENT);
                food0.setBackgroundColor(Color.TRANSPARENT);
                food3.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        /*
        toy0.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


            }

        }); */
        toy0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                toy=0;
                toy1.setBackgroundColor(Color.TRANSPARENT);
                toy2.setBackgroundColor(Color.TRANSPARENT);
                toy3.setBackgroundColor(Color.TRANSPARENT);
                toy0.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        toy1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!t[1]) {
                    if (cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        toy1.setImageResource(R.drawable.btennisball);
                        t[1] = true;
                        if(!mute) chaching.start();

                    }
                }
            }

        });
        toy1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!t[1]) return true;
                toy=1;
                toy0.setBackgroundColor(Color.TRANSPARENT);
                toy2.setBackgroundColor(Color.TRANSPARENT);
                toy3.setBackgroundColor(Color.TRANSPARENT);
                toy1.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        toy2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!t[2]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        toy2.setImageResource(R.drawable.bbasketball);
                        t[2] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        toy2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!t[2]) return true;
                toy=2;
                toy1.setBackgroundColor(Color.TRANSPARENT);
                toy0.setBackgroundColor(Color.TRANSPARENT);
                toy3.setBackgroundColor(Color.TRANSPARENT);
                toy2.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        toy3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!t[3]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        toy3.setImageResource(R.drawable.brainbowball);
                        t[3] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        toy3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!t[3]) return true;
                toy=3;
                toy1.setBackgroundColor(Color.TRANSPARENT);
                toy2.setBackgroundColor(Color.TRANSPARENT);
                toy0.setBackgroundColor(Color.TRANSPARENT);
                toy3.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });/*
        tool0.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


            }

        });*/
        tool0.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                tool=0;
                tool1.setBackgroundColor(Color.TRANSPARENT);
                tool2.setBackgroundColor(Color.TRANSPARENT);
                tool3.setBackgroundColor(Color.TRANSPARENT);
                tool0.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        tool1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!c[1]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        tool1.setImageResource(R.drawable.bgreenbrush);
                        c[1] = true;
                        if(!mute) chaching.start();
                    }

                }
            }

        });
        tool1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!c[1]) return true;
                tool=1;
                tool0.setBackgroundColor(Color.TRANSPARENT);
                tool2.setBackgroundColor(Color.TRANSPARENT);
                tool3.setBackgroundColor(Color.TRANSPARENT);
                tool1.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        tool2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!c[2]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        tool2.setImageResource(R.drawable.bredbrush);
                        c[2] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        tool2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!c[2]) return true;
                tool=2;
                tool1.setBackgroundColor(Color.TRANSPARENT);
                tool0.setBackgroundColor(Color.TRANSPARENT);
                tool3.setBackgroundColor(Color.TRANSPARENT);
                tool2.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
        tool3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(!c[3]){
                    if(cash < 100) {

                    } else {
                        cash -= 100;
                        String amount = Integer.toString(cash);
                        money.setText("   " + amount);
                        tool3.setImageResource(R.drawable.cookie2);
                        tool3.setImageResource(R.drawable.brainbowbrush);
                        c[3] = true;
                        if(!mute) chaching.start();
                    }
                }
            }

        });
        tool3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!c[3]) return true;
                tool=3;
                tool1.setBackgroundColor(Color.TRANSPARENT);
                tool2.setBackgroundColor(Color.TRANSPARENT);
                tool0.setBackgroundColor(Color.TRANSPARENT);
                tool3.setBackgroundColor(Color.BLACK);
                if(!mute) select.start();
                return true;
            }
        });
    }


}
