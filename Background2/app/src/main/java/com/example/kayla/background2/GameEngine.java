package com.example.kayla.background2;

import android.app.Activity;
import android.util.Log;

/**
 * Created by jimmychen on 4/16/16.
 */
public class GameEngine implements Runnable{

    //references needed in the game engine.
    private GameMode gameMode;
    private MainActivity mainActivity;

    volatile boolean playing;

    private long timeElapsed;
    private long startTime;

    private long speedConstant;

    Thread gameThread = null;

    public GameEngine(GameMode gameMode, MainActivity mainActivity){
        this.gameMode = gameMode;
        this.mainActivity = mainActivity;
        startTime = System.currentTimeMillis();
        speedConstant = 1;
    }

    public void run(){
        while(playing){
            timeElapsed = System.currentTimeMillis() - startTime;
            if(timeElapsed > 6000){
                startTime = System.currentTimeMillis();
                update( (int) timeElapsed/1000, (int) speedConstant);
            }
        }
    }

    public void update(int timeElapsed, int sim_speed){
        gameMode.timeElapsed(timeElapsed, sim_speed);
        mainActivity.runOnUiThread(new Runnable(){
            @Override
            public void run() {
                mainActivity.updateGameMode();
            }
        });
    }

    public void pause(){
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }
    }

    public void resume(){
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void setSpeedConstant(int speedConstant){
        this.speedConstant = speedConstant;
        //time increment to update everything in game (in milliseconds)
        //1x = 6s (normal speed) stats respond as if for every 6 s
        //10x = 60s stats respond as if 60s passed
        //100x = 600s stats respond as if 600s passed
    }

}
