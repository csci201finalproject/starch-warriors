package com.example.kayla.background2;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Observable;

/**
 * Created by jimmychen on 4/12/16.
 */
public class Pet extends Observable{
    private ImageView potatoImage;
    static String petName;


    public AttackMove fireAttack;
    public AttackMove waterAttack;
    public AttackMove grassAttack;
    private int[] aMoveLvls;
    private int[] aMoveDmg;

    private int battleHealth = 100;
    private int level = 1;
    private int exp = 0;

    private float health = 0.5f;
    private Stat hungriness;
    private Stat cleanliness;
    private Stat fun;

    private boolean canBattle = true;
    private boolean isFighting = true;
    private boolean isSick = false;
    private boolean isDirty = false;
    private boolean isHungry = false;
    private boolean isBored = false;

    private boolean isPlaying = false;
    private boolean isCleaning = false;
    private boolean isEating = false;
    private boolean isHealing = false;

    AnimationDrawable potatoAnimation;

    //0 is default happy potato
    //1 is hungry
    //2 is bored
    //3 is smelly
    //4 is sad (low health)
    private int currentAnimation;

    private MainActivity mainActivity;

    public Pet(ImageView imageView, MainActivity mainActivity, String name){
        potatoImage = imageView;
        fireAttack = new AttackMove(this, 1);
        waterAttack = new AttackMove(this, 2);
        grassAttack = new AttackMove(this, 3);

        hungriness = new Stat();
        cleanliness = new Stat();
        fun = new Stat();

        this.mainActivity = mainActivity;
        currentAnimation = 0;

        petName = name;
    }

    public void startAnimation(int animation) {
        currentAnimation = animation;
        mainActivity.startAnimation(animation);

    }

    public void feed(float inc){
        hungriness.increaseStat(inc);
        update();
        isEating = true;
    }

    public void play(float inc){
        fun.increaseStat(inc);
        update();
        isPlaying = true;
    }

    public void clean(float inc){
        cleanliness.increaseStat(inc);
        update();
        isCleaning = true;
    }

    public void update(){
        health = (getHungriness() + getCleanliness() + getFun())/3.0f;
        if (getHealth() < 0.3f) {
            startAnimation(4);
        } else if (getHungriness() < 0.3f) {
            startAnimation(1);
        } else if (getFun() < 0.3f) {
            startAnimation(2);
        } else if (getCleanliness() < 0.3f) {
            startAnimation(3);
        } else{
            if(currentAnimation != 0){
                startAnimation(0);
            }
        }
    }

    public void heal(){
        isSick = false;
        health = 0.5f;
        //default stat is 0.5f
        hungriness.setDefault();
        cleanliness.setDefault();
        fun.setDefault();
    }

    public int getBattleHealth() { return battleHealth; }

    public void levelUp() {
        level ++;
        battleHealth += 20;
        setChanged();
        notifyObservers(level);
    }

    public void levelAttack(int element) {
        if(element == 1)
            fireAttack.levelUp();
        else if (element == 2)
            waterAttack.levelUp();
        else if (element == 3)
            grassAttack.levelUp();
    }

    public float getHealth(){ return health;}
    public float getHungriness(){ return hungriness.getValue();}
    public float getCleanliness(){ return cleanliness.getValue();}
    public float getFun(){ return fun.getValue();}
    public int getLevel() {return level;}
    public int getCurrentAnimation(){ return currentAnimation;}


}
