package com.example.kayla.background2;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Bobo on 4/15/2016.
 */
public class BattleWaitActivity extends Activity {

    Button back;
    private boolean mute;
    MediaPlayer battlewaitsong;
    private String name;
    private int level;
    private int firelvl, waterlvl, grasslvl;
    //private int firedmg, waterdmg, grassdmg;
    private int battleHealth;
    private String enemyPetName = null;
    private int enemyHealth = 0;
    private int enemyLvl, enemyFireLvl, enemyWaterLvl, enemyGrassLvl;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battle_waiting);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mute = getIntent().getBooleanExtra("mute", false);
        setContentView(R.layout.battle_waiting);
        addListenerOnButton();

        //getting all the data for battlescreen
        Bundle extras = getIntent().getExtras();
        name = extras.getString("name");
        level = extras.getInt("level");
        battleHealth = extras.getInt("battleHealth");
        firelvl = getIntent().getIntExtra("firelvl", 0);
        waterlvl =getIntent().getIntExtra("waterlvl", 0);
        grasslvl =getIntent().getIntExtra("grasslvl", 0);
        //firedmg = extras.getInt("fireDmg");
        //waterdmg = extras.getInt("waterDmg");
        //grassdmg = extras.getInt("earthDmg");


        battlewaitsong = MediaPlayer.create(this, R.raw.battlewait);
        if(!mute) battlewaitsong.start();
        battlewaitsong.setLooping(true);

        //client / server thing
        Thread client = new Thread(){
            public void run(){
                try {
                    Intent battle = new Intent(getApplicationContext(), BattleActivity.class);
                    Socket s = null;
                    try {
                        s = new Socket(LoginActivity.IPaddress, 6789);
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                .getOutputStream())), true);
                        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                        // WHERE YOU ISSUE THE COMMANDS
                        Log.d("ClientActivity", "C: Sending command.");
                        out.println("Battle");
                        Log.d("ClientActivity", "C: Sent battle request.");
                        String response = in.readLine();
                        Log.d("ClientActivity", "C: Received connection ");
                        if(response != null & response.equals("Connected")) {
                            out.println("Client Information;" + "n:" + name +
                            " lvl:" + String.valueOf(level) +
                            " bhealth:" + String.valueOf(battleHealth) +
                            " flvl:" + String.valueOf(firelvl) +
                            " wlvl:" + String.valueOf(waterlvl) +
                            " glvl:" + String.valueOf(grasslvl));
                        }
                        Log.d("ClientActivity", "C: Sending enemy read request");
                        enemyPetName = in.readLine();
                        enemyHealth = Integer.parseInt(in.readLine());
                        enemyLvl = Integer.parseInt(in.readLine());
                        enemyFireLvl = Integer.parseInt(in.readLine());
                        enemyWaterLvl = Integer.parseInt(in.readLine());
                        enemyGrassLvl = Integer.parseInt(in.readLine());
                        Log.d("ClientActivity", "C: Received enemy info:" + enemyPetName + enemyHealth);
                        if(enemyPetName != null && enemyHealth != 0) {
                            battle.putExtra("mute", mute);
                            battle.putExtra("battleHealth", battleHealth);
                            battle.putExtra("elevel", enemyLvl);
                            battle.putExtra("level", level);
                            battle.putExtra("name", name);
                            battle.putExtra("firelvl", firelvl);
                            battle.putExtra("waterlvl", waterlvl);
                            battle.putExtra("grasslvl", grasslvl);
                            battle.putExtra("efirelvl", enemyFireLvl);
                            battle.putExtra("ewaterlvl", enemyWaterLvl);
                            battle.putExtra("egrasslvl", enemyGrassLvl);
                            battle.putExtra("enemyHealth", enemyHealth);
                            battle.putExtra("enemyName", enemyPetName);
                            startActivity(battle);
                        }
                    } catch (Exception e) {
                        showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                        Log.e("ClientActivity", "S: Error", e);
                    }
                    if(s != null)
                        s.close();
                } catch (Exception e) {
                    Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                }
            }
        };
        client.start();

    }

    @Override
    protected void onPause(){
        super.onPause();
        battlewaitsong.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(!mute) battlewaitsong .start();

    }

    public void sendPetInfo(Pet pet) {

    }

    public void receivePetInfo(Pet pet) {

    }

    public void addListenerOnButton() {

        back = (Button) findViewById(R.id.battleback);

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("screen", "battle");
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                finish();

            }

        });

    }

    public void showAlertDialog(String title, String message, int drawable_icon) {
        final String alert_title = title;
        final String alert_message = message;
        final int alert_icon = drawable_icon;
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new AlertDialog.Builder(BattleWaitActivity.this)
                        .setTitle(alert_title)
                        .setMessage(alert_message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(alert_icon)
                        .show();
            }
        });

    }
}
