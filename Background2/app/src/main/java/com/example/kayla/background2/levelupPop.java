package com.example.kayla.background2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by Bobo on 4/18/2016.
 */
public class levelupPop extends Activity {
    private ImageButton fire, earth, water;
    private AnimationDrawable playerPotatoAnimation;
    private ImageView playerPotatoImage;
    private CustomTextView levelMsg;
    private boolean mute = false;
    private int level;
    private String name;
    MediaPlayer levelupsong;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.levelup_popup);
        initializePets();
        initializeButtons();
        addListenerOnButtons();
        Bundle extras = getIntent().getExtras();
        mute = extras.getBoolean("mute");
        level = extras.getInt("level");
        name = extras.getString("name");
        levelMsg = (CustomTextView) findViewById(R.id.petLevel);
        levelMsg.setText(name + " is now level " + Integer.toString(level));
        levelupsong = MediaPlayer.create(this, R.raw.level_up);
        levelupsong.start();
    }

    private void initializePets(){

        playerPotatoImage = (ImageView) findViewById(R.id.leveledpotato);
        playerPotatoImage.setBackgroundResource(R.drawable.fun_potato_animation);
        //adjusts the size of the potato
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) playerPotatoImage.getLayoutParams();
        params.width = 200;
        params.height = 200;
        playerPotatoAnimation = (AnimationDrawable) playerPotatoImage.getBackground();

        playerPotatoImage.setLayoutParams(params);

        playerPotatoImage.post(new Runnable() {
            @Override
            public void run() {
                playerPotatoAnimation.start();
            }
        });
    }

    private void initializeButtons() {
        fire = (ImageButton) findViewById(R.id.fire);
        water = (ImageButton) findViewById(R.id.water);
        earth = (ImageButton) findViewById(R.id.earth);
    }

    private void addListenerOnButtons() {
        fire.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent levelup = new Intent(getApplicationContext(), MainActivity.class);
                levelup.putExtra("screen", "levelup");
                levelup.putExtra("attacklvlup", 1);
                levelup.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(levelup);
            }
        });
        water.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent levelup = new Intent(getApplicationContext(), MainActivity.class);
                levelup.putExtra("screen", "levelup");
                levelup.putExtra("attacklvlup", 2);
                levelup.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(levelup);
            }
        });
        earth.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent levelup = new Intent(getApplicationContext(), MainActivity.class);
                levelup.putExtra("screen", "levelup");
                levelup.putExtra("attacklvlup", 3);
                levelup.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(levelup);
            }
        });
    }
}
