package com.example.kayla.background2;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.app.Fragment;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    private AnimationDrawable cloudAnimation, potatoAnimation;
    private ImageView potatoImage;

    private GameEngine gameEngine;

    private Pop menu;
    private Pet pet;
    private String screen;
    private Food food;
    private CleaningTool cleaningTool;
    private Toy toy;
    private ImageView foodImage;
    private ImageView toyImage;
    private ImageView cleaningImage;
    private int cash, foodSelect=0, toySelect=0, toolSelect=0;
    private Button menuButton;
    private boolean mute = false;
    private ImageButton muteButton;
    private int attackToLvl;

    MediaPlayer themesong;

    GameMode gameMode;

    private String pet_name;
    private int[] animationIDs = new int[5];



    @Override
    public void onBackPressed() {
        return;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        screen = intent.getStringExtra("screen");
        if(screen.equals("shop")) {
            screen = "";
            cash = intent.getIntExtra("shopCash", 1000);
            foodSelect = intent.getIntExtra("food", 0);
            switch(foodSelect) {
                case 0:  foodImage.setImageResource(R.drawable.burger);
                    break;
                case 1:  foodImage.setImageResource(R.drawable.pizza);
                    break;
                case 2:  foodImage.setImageResource(R.drawable.hotdog);
                    break;
                case 3:  foodImage.setImageResource(R.drawable.slushie);
                    break;
            }
            toySelect = intent.getIntExtra("toy", 0);
            switch(toySelect) {
                case 0:  toyImage.setImageResource(R.drawable.baseball);
                    break;
                case 1:  toyImage.setImageResource(R.drawable.tennisball);
                    break;
                case 2:  toyImage.setImageResource(R.drawable.basketball);
                    break;
                case 3:  toyImage.setImageResource(R.drawable.rainbowball);
                    break;
            }
            toolSelect = intent.getIntExtra("tool", 0);
            switch(toolSelect) {
                case 0:  cleaningImage.setImageResource(R.drawable.bluebrush);
                    break;
                case 1:  cleaningImage.setImageResource(R.drawable.greenbrush);
                    break;
                case 2:  cleaningImage.setImageResource(R.drawable.redbrush);
                    break;
                case 3:  cleaningImage.setImageResource(R.drawable.rainbowbrush);
                    break;
            }
        }
        else if(screen.equals("levelup")) {
            screen = "";
            attackToLvl = intent.getIntExtra("attacklvlup", 0);
            pet.levelAttack(attackToLvl);
        }
        else if(screen.equals("loss")) {
            cash -= 50;
            gameMode.addBattleExp(30);
        }
        else if(screen.equals("win")) {
            cash += 100;
            gameMode.addBattleExp(50);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
        settingBackground();

        initializePet();

        setupAnimation();

        initializeVariables();

        addActions();

    }

    private void settingBackground(){
        ImageView cloudImage = (ImageView) findViewById(R.id.grass);
        assert cloudImage != null;
        cloudImage.setBackgroundResource(R.drawable.clouds_animation);

        cloudAnimation = (AnimationDrawable) cloudImage.getBackground();
        cloudImage.post(new Runnable() {
                            @Override
                            public void run() {
                                cloudAnimation.start();
                            }
                        });
    }

    private void initializePet(){

        potatoImage = (ImageView) findViewById(R.id.happy_potato);
        //adjusts the size of the potato
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) potatoImage.getLayoutParams();
        params.width = 120;
        params.height = 120;

        potatoImage.setLayoutParams(params);
        potatoAnimation = (AnimationDrawable) potatoImage.getBackground();

        potatoImage.post(new Runnable() {
            @Override
            public void run() {
                potatoAnimation.start();
            }
        });



        animationIDs[0] = R.drawable.happy_potato_animation;
        animationIDs[1] = R.drawable.hungry_potato_animation;
        animationIDs[2] = R.drawable.bored_potato_animation;
        animationIDs[3] = R.drawable.smelly_potato_animation;
        animationIDs[4] = R.drawable.sad_potato_animation;
        pet_name = getIntent().getStringExtra("name");
        //Log.v("pet_name", "Pet name: " + pet_name);
        pet = new Pet(potatoImage, this, pet_name); //hopefully won't overlap with the animation
        pet.addObserver(this);
    }

    private void setupAnimation(){
        ImageView potatoImage = (ImageView) findViewById(R.id.happy_potato);
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) potatoImage.getLayoutParams();
        params.width = 120;
        params.height = 120;
        potatoImage.requestLayout();
//        potatoImage.setBackgroundResource(R.drawable.happy_potato_animation);
//// existing height is ok as is, no need to edit it
//        potatoImage.setLayoutParams(params);
//        potatoAnimation = (AnimationDrawable) potatoImage.getBackground();
//        potatoAnimation.start();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            pet.startAnimation(pet.getCurrentAnimation());
        }
    }

    private void initializeVariables(){

        foodImage = (ImageView) findViewById(R.id.food_image);
        toyImage = (ImageView) findViewById(R.id.toy_image);
        cleaningImage = (ImageView) findViewById(R.id.cleaning_image);
        cash = 1000;

        food = new Food("chicken", foodImage);
        food.select();
        food.buyItem();
        toy = new Toy("ball", toyImage);
        toy.select();
        toy.buyItem();
        cleaningTool = new CleaningTool("brush", cleaningImage);
        cleaningTool.select();
        cleaningTool.buyItem();

        themesong = MediaPlayer.create(this, R.raw.theme);
        if(!mute) themesong.start();
        themesong.setLooping(true);


        gameMode = new GameMode(pet);
        gameEngine = new GameEngine(gameMode, this);
        updateGameMode();
    }

    private void addActions(){
        foodImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if (view == findViewById(R.id.food_image)) {
                    gameMode.feed();
                    toastStatChange("Hungriness", pet_name, pet.getHungriness());
                    updateGameMode();
                }
            }
        });

        toyImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                gameMode.play();
                toastStatChange("Fun", pet_name, pet.getFun());
                updateGameMode();
            }
        });

        cleaningImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                gameMode.clean();
                toastStatChange("Cleanliness", pet_name, pet.getCleanliness());
                updateGameMode();
            }
        });

        Button speed_10_button = (Button) findViewById(R.id.speed_10x);
        speed_10_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                gameEngine.setSpeedConstant(1);
            }
        });

        Button speed_100_button = (Button) findViewById(R.id.speed_100x);
        speed_100_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                gameEngine.setSpeedConstant(10);
            }
        });


//        //testing
//        Button subtractHungriness = (Button) findViewById(R.id.subtract_hungriness);
//        subtractHungriness.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                Food.feed(pet, -0.1f);
//                toastStatChange("Hungriness", pet_name, pet.getHungriness());
//                //potatoImage.setBackgroundResource(R.drawable.hungry_potato_animation);
//                //((AnimationDrawable) potatoImage.getBackground() ).start();
//                updateGameMode();
//            }
//        });
//
//        Button subtractFun = (Button) findViewById(R.id.subtract_fun);
//        subtractFun.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                Toy.play(pet, -0.1f);
//                toastStatChange("Fun", pet_name, pet.getFun());
//                updateGameMode();
//            }
//        });
//
//        Button subtractClean = (Button) findViewById(R.id.subtract_clean);
//        subtractClean.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View view){
//                CleaningTool.clean(pet, -0.1f);
//                toastStatChange("Cleanliness", pet_name, pet.getCleanliness());
//                updateGameMode();
//            }
//        });


    }

    public void updateGameMode(){
//        TextView lvl = (TextView) findViewById(R.id.lvl);
//        String lvl_str = getResources().getString(R.string.TextView_level) + " " + gameMode.getLvl();
//        lvl.setText(lvl_str);
//
//        TextView expNeeded = (TextView) findViewById(R.id.expNeeded);
//        String expNeeded_str = getResources().getString(R.string.TextView_expNeeded) + " " + gameMode.getExpNeeded();
//        expNeeded.setText(expNeeded_str);
//
//        TextView health = (TextView) findViewById(R.id.health);
//        String health_str = getResources().getString(R.string.TextView_health) + " " + pet.getHealth();
//        health.setText(health_str);

        TextView stamina = (TextView) findViewById(R.id.stamina);
        String stamina_str = getResources().getString(R.string.TextView_stamina) + " " + gameMode.getStamina();
        stamina.setText(stamina_str);
    }


    public void startAnimation(int animation){
        final int id = animation;
        potatoImage.post(new Runnable() {
            @Override
            public void run() {
                potatoImage.setBackgroundResource(animationIDs[id]);
                potatoAnimation = (AnimationDrawable) potatoImage.getBackground();
                potatoAnimation.start();
            }
        });
    }


    public void toastStatChange(String statName, String name, float stat){
        final String feed_pet_string = getResources().getString(R.string.toast_feed_string)
                + " ";
        final String play_pet_string = getResources().getString(R.string.toast_play_string)
                + " ";
        final String clean_pet_string = getResources().getString(R.string.toast_clean_string)
                + " ";

        String toastString = "Uninitialized String";
        if(statName.equals("Hungriness")) {
            toastString = feed_pet_string + name;
        }
        else if(statName.equals("Fun")) {
            toastString = play_pet_string + name;
        }
        else if(statName.equals("Cleanliness")) {
            toastString = clean_pet_string + name;
        }

        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_SHORT).show();
    }

    protected void onResume(){
        super.onResume();
        if(!mute)
            themesong.start();
        gameEngine.resume();
    }

    protected void onPause(){
        super.onPause();
        themesong.pause();
        gameEngine.pause();
    }


    public void addListenerOnButton() {

        menuButton = (Button) findViewById(R.id.menu);
        muteButton = (ImageButton) findViewById(R.id.imageButton);

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(menu != null)
                    getSupportFragmentManager().beginTransaction().remove(menu).commit();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                Bundle bundle =new Bundle();
                bundle.putInt("cash", cash);
                bundle.putFloat("health",pet.getHealth());
                bundle.putFloat("hungriness",pet.getHungriness());
                bundle.putFloat("fun",pet.getFun());
                bundle.putFloat("cleanliness",pet.getCleanliness());
                bundle.putInt("level",gameMode.getLvl());
                bundle.putInt("expNeeded",gameMode.getExpNeeded());
                bundle.putString("name",pet.petName);
                bundle.putBoolean("mute", mute);

                //adding stuff for battlescreen calculations
                bundle.putInt("battleHealth", pet.getBattleHealth());
                bundle.putInt("fire", pet.fireAttack.getLevel());
                bundle.putInt("water", pet.waterAttack.getLevel());
                bundle.putInt("earth", pet.grassAttack.getLevel());
                //bundle.putInt("fireDmg", pet.fireAttack.calculatePower());
                //bundle.putInt("waterDmg", pet.waterAttack.calculatePower());
                //bundle.putInt("earthDmg", pet.grassAttack.calculatePower());



                menu = new Pop();
                menu.setArguments(bundle);
                transaction.add(R.id.main_layout, menu);
                transaction.commit();
            }

        });

        muteButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(mute) {
                    mute = false;
                    themesong.start();
                    muteButton.setImageResource(R.drawable.speaker);
                } else {
                    mute = true;
                    themesong.pause();
                    muteButton.setImageResource(R.drawable.mute);
                }
            }

        });

    }

    @Override
    public void update(Observable observable, Object o) {
        Intent levelup = new Intent(getApplicationContext(), levelupPop.class);
        levelup.putExtra("mute", mute);
        levelup.putExtra("name", pet_name);
        levelup.putExtra("level", pet.getLevel());
        startActivity(levelup);
    }
}
