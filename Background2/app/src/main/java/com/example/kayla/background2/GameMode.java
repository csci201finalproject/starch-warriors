package com.example.kayla.background2;

import android.util.Log;

/**
 * Created by jimmychen on 4/14/16.
 */
public class GameMode {

    //turn into array for multiple pets
    private int exp=0;
    private int lvl=1;
    private Pet pet;


    private int money=0;

    //Experience needed until next level
    //this is modeled using the equation
    //expNeeded = baseXP * lvl ^ factor
    private int expNeeded;
    //stamina is number of clicks the user can perform at this given time
    private int stamina;

    //maxClicks is used to calculate the stat increase
    private float maxClicks;

    private final int baseXP = 45;
    private final int factor = 2;
    //linear constant for stat decrease
    private final double decreaseConstant = -0.0000142;

    private int foodClicks=0;
    private int playClicks=0;
    private int cleanClicks=0;


    public GameMode(Pet pet){
        nextLevel();
        this.pet = pet;
    }

    private void nextLevel(){
        expNeeded = baseXP * ((int) Math.pow((double)lvl, (double)factor));
        stamina = 5*lvl + 5;
        maxClicks = ((int) (1.2f* (float) lvl)) + 3;
        resetClicks();
    }

    private void resetClicks(){
        foodClicks = 0;
        playClicks = 0;
        cleanClicks = 0;
    }

    public void feed(){
        if(action(foodClicks)) {
            foodClicks++;
            float diff = 1.0f - pet.getHungriness();
            if (diff > 0.5f) {
                diff = 0.5f;
            }
            float statChange = diff / ((float) Math.pow((double) maxClicks, 2)) *
                    (-(float) foodClicks + 2 * maxClicks);
            System.out.println("Stat Change: " + statChange);
            pet.feed(statChange);
            addExp(statChange);
        }
    }

    public void play(){
        if(action(playClicks)) {
            playClicks++;
            float diff = 1.0f - pet.getFun();
            if (diff > 0.5f) {
                diff = 0.5f;
            }
            float statChange = diff / ((float) Math.pow((double) maxClicks, 2)) *
                    (-(float) playClicks + 2 * maxClicks);
            System.out.println("Stat Change: " + statChange);
            pet.play(statChange);
            addExp(statChange);
        }
    }

    public void clean(){
        if(action(cleanClicks)) {
            cleanClicks++;
            float diff = 1.0f - pet.getCleanliness();
            if (diff > 0.5f) {
                diff = 0.5f;
            }
            float statChange = diff / ((float) Math.pow((double) maxClicks, 2)) *
                    (-(float) cleanClicks + 2 * maxClicks);
            System.out.println("Stat Change: " + statChange);
            pet.clean(statChange);
            addExp(statChange);
        }
    }

    private boolean action(int clicks){
        if( stamina != 0 & (clicks != (int) maxClicks)) {
            stamina--;
            return true;
        }
        return false;
    }

    public void addExp(float statChange){
        expNeeded -= (int) (statChange * 50.);
        if(expNeeded <= 0){
            lvl++;
            nextLevel();
            pet.levelUp();
        }
    }

    public void addBattleExp(int exp) {
        expNeeded -= exp;
        if(expNeeded <= 0){
            lvl++;
            nextLevel();
            pet.levelUp();
        }
    }

    public void timeElapsed(int timeElapsed, int sim_speed){
        if(stamina < (5*lvl))
            stamina += 1;
        foodClicks -= sim_speed;
        if(foodClicks < 0){
            foodClicks = 0;
        }
        playClicks -= sim_speed;
        if(playClicks < 0){
            playClicks = 0;
        }
        cleanClicks -= sim_speed;
        if(cleanClicks < 0){
            cleanClicks = 0;
        }
        float statChange = (float) ( (double) timeElapsed * decreaseConstant * (double)sim_speed * 50);
        float testStatChange = (float) ( (double) timeElapsed * decreaseConstant);
        //Log.v("statChange", "statChange: " + Float.toString(statChange) + " testStatChange: " + Float.toString(testStatChange));

        pet.feed(statChange);
        pet.play(statChange);
        pet.clean(statChange);
    }

    public int getLvl(){ return lvl;}
    public int getExpNeeded(){ return expNeeded;}
    public int getStamina(){ return stamina;}
}
