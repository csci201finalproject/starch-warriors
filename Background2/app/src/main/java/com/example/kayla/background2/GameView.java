package com.example.kayla.background2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by jimmychen on 4/10/16.
 */
public class GameView extends SurfaceView implements Runnable{

    Thread gameThread = null;

    SurfaceHolder ourHolder;

    // A boolean which we will set and unset
    // when the game is running- or not.
    volatile boolean playing;

    Canvas canvas;
    Paint paint;

    long fps;

    //calculating the fps
    private long timeThisFrame;

    Bitmap bitmapHappyPotato;

    // starts off not moving
    boolean isMoving = false;
    // Action running 150 pixels per second
    float actionSpeedPerSecond = 150;
    // Starting 10 pixels from the bottom
    float potatoYPosition = 500;
    float potatoXPosition = 500;

    //frame size
    private int frameWidth = 100;
    private int frameHeight = 100;

    //number of frames in the sprite sheet
    private int frameCount = 8;
    //starting frame
    private int currentFrame = 0;
    //lenght of time since last frame change
    private long lastFrameChangeTime = 0;
    //length of each frame
    private int frameLengthInMilliseconds = 100;

    // A rectangle to define an area of the
    // sprite sheet that represents 1 frame
    private Rect frameToDraw = new Rect(
            0,
            0,
            frameWidth,
            frameHeight);

    // A rect that defines an area of the screen
    // on which to draw
    RectF whereToDraw = new RectF(
            potatoXPosition, potatoYPosition,
            potatoXPosition+frameWidth,
            potatoYPosition + frameHeight);

    public GameView(Context context) {
        super(context);

        ourHolder = getHolder();
        paint = new Paint();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        bitmapHappyPotato = BitmapFactory.decodeResource(this.getResources(), R.drawable.happy_potato
        ,options);

        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;
        // Scale the bitmap to the correct size
        // We need to do this because Android automatically
        // scales bitmaps based on screen density
        bitmapHappyPotato = Bitmap.createScaledBitmap(bitmapHappyPotato,
                frameWidth * frameCount,
                frameHeight,
                false);
    }

    public void getCurrentFrame(){
        long time  = System.currentTimeMillis();
        if(isMoving) {// Only animate if bob is moving
            if ( time > lastFrameChangeTime + frameLengthInMilliseconds) {
                lastFrameChangeTime = time;
                currentFrame ++;
                if (currentFrame >= frameCount) {
                    currentFrame = 0;
                }
            }
        }
        //update the left and right values of the source of
        //the next frame on the spritesheet
        frameToDraw.left = currentFrame * frameWidth;
        frameToDraw.right = frameToDraw.left + frameWidth;
    }

    @Override
    public void run() {
        while(playing) {

            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            // Update the frame
            update();

            // Draw the frame
            draw();

            // Calculate the fps this frame
            // We can then use the result to
            // time animations and more.
            timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame > 0) {
                fps = 1000 / timeThisFrame;
            }
        }
    }

    public void update() {
        if(isMoving){
            //potatoYPosition = potatoYPosition + (actionSpeedPerSecond / fps);
        }
    }

    public void draw() {
        // Make sure our drawing surface is valid or we crash
        if (ourHolder.getSurface().isValid()) {
            // Lock the canvas ready to draw
            // Make the drawing surface our canvas object
            canvas = ourHolder.lockCanvas();

            // Draw the background color
            canvas.drawColor(Color.argb(255,  26, 128, 182));

            // Choose the brush color for drawing
            paint.setColor(Color.argb(255,  249, 129, 0));

            // Make the text a bit bigger
            paint.setTextSize(45);

            // Display the current fps on the screen
            canvas.drawText("FPS:" + fps, 20, 40, paint);

            // Draw bob at bobXPosition, 200 pixels
            //canvas.drawBitmap(bitmapHappyPotato, 400, potatoYPosition, paint);
            whereToDraw.set(potatoXPosition, potatoYPosition,
                    potatoXPosition+frameWidth,
                    potatoYPosition + frameHeight);

            getCurrentFrame();

            canvas.drawBitmap(bitmapHappyPotato,
                    frameToDraw,
                    whereToDraw, paint);


            // Draw everything to the screen
            // and unlock the drawing surface
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    // If SimpleGameEngine Activity is paused/stopped
    // shutdown our thread.
    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

    }

    // If SimpleGameEngine Activity is started theb
    // start our thread.
    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    // The SurfaceView class implements onTouchListener
    // So we can override this method and detect screen touches.
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            // Player has touched the screen
            case MotionEvent.ACTION_DOWN:

                // Set isMoving so Bob is moved in the update method
                isMoving = true;

                break;

            // Player has removed finger from screen
            case MotionEvent.ACTION_UP:

                // Set isMoving so Bob does not move
                isMoving = false;

                break;
        }
        return true;
    }

}