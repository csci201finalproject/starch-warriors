package com.example.kayla.background2;


import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Bobo on 4/16/2016.
 */

public class Pop extends DialogFragment {
    private Button shopButton, battleButton, statsButton, logoutButton,
            exitButton1, exitButton2, exitButton3, exitButton4;


    //stats screen data
    float health, hungriness, fun, cleanliness;
    int level, expNeeded;
    String name;

    private int cash;
    private boolean mute;

    private int firelvl, waterlvl, grasslvl;
    //private int firedmg, waterdmg, grassdmg;
    private int battleHealth;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        cash = extras.getInt("cash");
        mute = extras.getBoolean("mute");

        health = extras.getFloat("health");
        hungriness = extras.getFloat("hungriness");
        fun = extras.getFloat("fun");
        cleanliness = extras.getFloat("cleanliness");
        level = extras.getInt("level");
        expNeeded = extras.getInt("expNeeded");
        name = extras.getString("name");

        //battlescreen needed stuff
        battleHealth = extras.getInt("battleHealth");
        firelvl = extras.getInt("fire");
        waterlvl = extras.getInt("water");
        grasslvl = extras.getInt("earth");
        //firedmg = extras.getInt("fireDmg");
        //waterdmg = extras.getInt("waterDmg");
        //grassdmg = extras.getInt("earthDmg");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle saveInstanceState){

        View rootView = inflater.inflate(R.layout.pop, container, false);
        shopButton = (Button) rootView.findViewById(R.id.shopButton);
        statsButton = (Button) rootView.findViewById(R.id.statsButton);
        battleButton = (Button) rootView.findViewById(R.id.battleButton);
        logoutButton = (Button) rootView.findViewById(R.id.logoutButton);
        exitButton1 = (Button) rootView.findViewById(R.id.button);
        exitButton2 = (Button) rootView.findViewById(R.id.button2);
        exitButton3 = (Button) rootView.findViewById(R.id.button3);
        exitButton4 = (Button) rootView.findViewById(R.id.button4);
        addListenerOnButton();
        return rootView;
    }

    private void addListenerOnButton() {
        shopButton.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent shop = new Intent(getActivity().getApplicationContext(), ShopActivity.class);
                shop.putExtra("cash", cash);
                shop.putExtra("mute", mute);
                shop.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();
                startActivity(shop);
            }
        });
        statsButton.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent stats = new Intent(getActivity().getApplicationContext(), StatisticsActivity.class);
                stats.putExtra("health",health);
                stats.putExtra("hungriness",hungriness);
                stats.putExtra("fun",fun);
                stats.putExtra("cleanliness",cleanliness);
                stats.putExtra("level",level);
                stats.putExtra("expNeeded",expNeeded);
                stats.putExtra("name",name);
                stats.putExtra("mute", mute);
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();

                startActivity(stats);
            }
        });
        battleButton.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent battle = new Intent(getActivity().getApplicationContext(), BattleWaitActivity.class);
                battle.putExtra("mute", mute);
                battle.putExtra("battleHealth", battleHealth);
                battle.putExtra("level", level);
                battle.putExtra("name", name);
                battle.putExtra("firelvl", firelvl);
                battle.putExtra("waterlvl", waterlvl);
                battle.putExtra("grasslvl", grasslvl);
                //battle.putExtra("fireDmg", firedmg);
                //battle.putExtra("waterDmg", waterdmg);
                //battle.putExtra("grassDmg", grassdmg);
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();
                startActivity(battle);
            }
        });
        logoutButton.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                Intent logout = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                logout.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ActivityCompat.finishAffinity(getActivity());
                startActivity(logout);

                /*
                Intent exit = new Intent(Intent.ACTION_MAIN);
                exit.addCategory(Intent.CATEGORY_HOME);
                exit.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(exit);*/
            }
        });

        //basically windowCloseOnTouchOutside
        exitButton1.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();
            }
        });
        exitButton2.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();
            }
        });
        exitButton3.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();
            }
        });
        exitButton4.setOnClickListener(new View.OnClickListener  () {
            @Override
            public void onClick(View arg0) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(Pop.this).commit();
            }
        });
    }
}
