package com.example.kayla.background2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class BattleActivity extends AppCompatActivity {

    MediaPlayer battlesong;

    private boolean mute = false;

    private ImageView redFiller;

    private AnimationDrawable playerPotatoAnimation, enemyPotatoAnimation;
    private ImageView playerPotatoImage, enemyPotatoImage;
    private ImageButton fireButton, waterButton, grassButton;
    private CustomTextView myPotato,enemyPotato;

    private CustomTextView  fireLevel, waterLevel, grassLevel;

    private String name1, name2;
    private int level1, level2;
    private int firelvl1, waterlvl1, grasslvl1, firelvl2, waterlvl2, grasslvl2;
    //private int firedmg1, waterdmg1, grassdmg1, firedmg2, waterdmg2, grassdmg2;
    private int[] attackDmg1;
    private int[] attackDmg2;
    private int battleHealth1,fullBattleHealth1, battleHealth2, fullBattleHealth2;
    private int element1, element2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_battle);
        mute = getIntent().getBooleanExtra("mute", false);
        initializePets();
        initializeButtons();
        addButtonActions();
        //initializeLabels();
        Bundle extras = getIntent().getExtras();
        name1 = extras.getString("name");
        level1 = extras.getInt("level");
        fullBattleHealth1 = extras.getInt("battleHealth");
        firelvl1 = getIntent().getIntExtra("firelvl", 0);
        waterlvl1 =getIntent().getIntExtra("waterlvl", 0);
        grasslvl1 =getIntent().getIntExtra("grasslvl", 0);
        firelvl2 = extras.getInt("efirelvl");
        waterlvl2 = extras.getInt("ewaterlvl");
        grasslvl2 = extras.getInt("egrasslvl");
        level2 = extras.getInt("elevel");
        fullBattleHealth2 = extras.getInt("enemyHealth");
        name2 = extras.getString("enemyName");
        attackDmg1 = new int[4];
        attackDmg1[1] = calculatePower(level1, firelvl1);
        attackDmg1[2] = calculatePower(level1, waterlvl1);
        attackDmg1[3] = calculatePower(level1, grasslvl1);
        attackDmg2 = new int[4];
        attackDmg2[1] = calculatePower(level2, firelvl2);
        attackDmg2[2] = calculatePower(level2, waterlvl2);
        attackDmg2[3] = calculatePower(level2, grasslvl2);
        battleHealth1 = fullBattleHealth1;
        battleHealth2 = fullBattleHealth2;
        setHealthP2(fullBattleHealth2);
        setHealthP1(fullBattleHealth1);
        initializeLabels();
        battlesong = MediaPlayer.create(this, R.raw.battle);
        if(!mute) battlesong.start();
        battlesong.setLooping(true);

        
/*
        //client stuff
        Thread client = new Thread(){
            public void run(){
                try {
                    Socket s = null;
                    try {
                        s = new Socket(LoginActivity.IPaddress, 6789);
                        PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                .getOutputStream())), true);
                        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                        // WHERE YOU ISSUE THE COMMANDS
                        Log.d("ClientActivity", "C: Sending command.");
                        out.println("Battle");
                        Log.d("ClientActivity", "C: Sent battle request.");
                        String response = in.readLine();

                    } catch (Exception e) {
                        showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                        Log.e("ClientActivity", "S: Error", e);
                    }
                    if(s != null)
                        s.close();
                } catch (Exception e) {
                    Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                }
            }
        };
        client.start();*/

    }

    //dishing out the damage after comparing the chosen elements
    private void compareElements(int element1, int element2) {
        if (element1==element2)
            damageTie(attackDmg1[element1], attackDmg2[element1]);
        else if (element1==1 && element2==2)
            p1Damaged(attackDmg2[2]);
        else if (element1==2 && element2==3)
            p1Damaged(attackDmg2[3]);
        else if (element1==3 && element2==1)
            p1Damaged(attackDmg2[1]);
        else if (element2==1 && element1==2)
            p2Damaged(attackDmg1[2]);
        else if (element2==2 && element1==3)
            p2Damaged(attackDmg1[3]);
        else if (element2==3 && element1==1)
            p2Damaged(attackDmg1[1]);

    }

    private void p1Damaged(int damage) {
        battleHealth1 -= damage;
        if(battleHealth1 <= 0) {
            setHealthP1(0);
            battleLose();
        }
        setHealthP1(battleHealth1);
    }

    private void p2Damaged(int damage) {
        battleHealth2 -= damage;
        if(battleHealth2 <= 0) {
            setHealthP2(0);
            battleWin();
        }
        setHealthP2(battleHealth2);
    }

    private void damageTie(int damage1, int damage2) {
        battleHealth1 -= damage2;
        battleHealth2 -= damage1;
        if(battleHealth2 <= 0) {
            setHealthP2(0);
            battleWin();
        }
        if(battleHealth1 <= 0) {
            setHealthP1(0);
            battleLose();
        }
        setHealthP1(battleHealth1);
        setHealthP2(battleHealth2);
    }

    private void setHealthP2(float health){
        redFiller = (ImageView)findViewById(R.id.p2Health);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) redFiller.getLayoutParams();
        params.width = (int)( 285.0f * ( health/ (float)fullBattleHealth2));
        System.out.println("Player 2: " + params.width);
        redFiller.setLayoutParams(params);
    }

    private void setHealthP1(float health){
        redFiller = (ImageView)findViewById(R.id.p1Health);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) redFiller.getLayoutParams();
        params.width = (int)( 285.0f * ( health/ (float)fullBattleHealth1));
        System.out.println("Player 1: " + params.width);
        redFiller.setLayoutParams(params);
    }

    private void initializeLabels() {
        enemyPotato = (CustomTextView) findViewById(R.id.TopPotato);
        myPotato = (CustomTextView) findViewById(R.id.BottomPotato);
        fireLevel = (CustomTextView)findViewById(R.id.fire_attack_level);
        waterLevel = (CustomTextView)findViewById(R.id.water_attack_level);
        grassLevel = (CustomTextView)findViewById(R.id.grass_attack_level);

        enemyPotato.setText(name2);
        myPotato.setText(name1);
        fireLevel.setText("Level " + String.valueOf(firelvl1));
        waterLevel.setText("Level " + String.valueOf(waterlvl1));
        grassLevel.setText("Level " + String.valueOf(grasslvl1));

    }

    private void initializeButtons() {

        waterButton = (ImageButton) findViewById(R.id.waterButton);

        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) waterButton.getLayoutParams();
        params.width = 300;
        params.height = 300;

        waterButton.setLayoutParams(params);



        fireButton = (ImageButton) findViewById(R.id.fireButton);

        ViewGroup.LayoutParams params2 = (ViewGroup.LayoutParams) fireButton.getLayoutParams();
        params2.width = 250;
        params2.height = 250;

        fireButton.setLayoutParams(params2);



        grassButton = (ImageButton) findViewById(R.id.grassButton);

        ViewGroup.LayoutParams params3 = (ViewGroup.LayoutParams) grassButton.getLayoutParams();
        params3.width = 300;
        params3.height = 300;

        grassButton.setLayoutParams(params3);
    }

    private void addButtonActions() {
        fireButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                element1 = 1;
                //send data here, and wait for response
                //client stuff
                Thread client = new Thread(){
                    public void run(){
                        try {
                            Socket s = null;
                            try {
                                s = new Socket(LoginActivity.IPaddress, 6789);
                                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                        .getOutputStream())), true);
                                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                                // WHERE YOU ISSUE THE COMMANDS
                                out.println("Attack");
                                Log.d("ClientActivity", "C: Sending command.");
                                out.println(String.valueOf(element1));
                                Log.d("ClientActivity", "C: Sent battle request.");
                                String response = in.readLine();
                                if(response != null) {
                                    element2 = Integer.parseInt(response);
                                }
                                updateHealth(element1, element2); s.close();
                            } catch (Exception e) {
                                showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                                Log.e("ClientActivity", "S: Error", e);
                            }
                            if(s != null)
                                s.close();
                        } catch (Exception e) {
                            Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                        }
                    }
                };
                client.start();
            }

        });
        waterButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                element1 = 2;
                //send data here, and wait for response
                //client stuff
                Thread client = new Thread(){
                    public void run(){
                        try {
                            Socket s = null;
                            try {
                                s = new Socket(LoginActivity.IPaddress, 6789);
                                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                        .getOutputStream())), true);
                                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                                // WHERE YOU ISSUE THE COMMANDS
                                out.println("Attack");
                                Log.d("ClientActivity", "C: Sending command.");
                                out.println(String.valueOf(element1));
                                Log.d("ClientActivity", "C: Sent battle request.");
                                String response = in.readLine();
                                if(response != null) {
                                    element2 = Integer.parseInt(response);
                                }
                                updateHealth(element1, element2); s.close();
                            } catch (Exception e) {
                                showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                                Log.e("ClientActivity", "S: Error", e);
                            }
                            if(s != null)
                                s.close();
                        } catch (Exception e) {
                            Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                        }
                    }
                };
                client.start();
            }

        });
        grassButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                element1 = 3;
                //send data here, and wait for response0
                Thread client = new Thread(){
                    public void run(){
                        try {
                            Socket s = null;
                            try {
                                s = new Socket(LoginActivity.IPaddress, 6789);
                                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s
                                        .getOutputStream())), true);
                                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                                // WHERE YOU ISSUE THE COMMANDS
                                out.println("Attack");
                                Log.d("ClientActivity", "C: Sending command.");
                                out.println(String.valueOf(element1));
                                Log.d("ClientActivity", "C: Sent battle request.");
                                String response = in.readLine();
                                if(response != null) {
                                    element2 = Integer.parseInt(response);
                                }
                                updateHealth(element1, element2); s.close();
                            } catch (Exception e) {
                                showAlertDialog("Connection","Connection error. Are you on the right network?",android.R.drawable.ic_dialog_alert);
                                Log.e("ClientActivity", "S: Error", e);
                            }
                            if(s != null)
                                s.close();
                        } catch (Exception e) {
                            Log.e("ClientActivity:", "Connecting to server ioe: " + e);
                        }
                    }
                };
                client.start();
            }

        });
    }

    @Override
    protected void onPause(){
        super.onPause();
        battlesong.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(!mute) battlesong.start();

    }

    private void initializePets(){

        playerPotatoImage = (ImageView) findViewById(R.id.player_potato);
        playerPotatoImage.setBackgroundResource(R.drawable.happy_potato_animation);
        //adjusts the size of the potato
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) playerPotatoImage.getLayoutParams();
        params.width = 200;
        params.height = 200;
        playerPotatoAnimation = (AnimationDrawable) playerPotatoImage.getBackground();

        playerPotatoImage.setLayoutParams(params);

        playerPotatoImage.post(new Runnable() {
            @Override
            public void run() {
               playerPotatoAnimation.start();
            }
        });


        enemyPotatoImage = (ImageView) findViewById(R.id.enemy_potato);
        enemyPotatoImage.setBackgroundResource(R.drawable.happy_potato_animation);
        //adjusts the size of the potato
        ViewGroup.LayoutParams params2 = (ViewGroup.LayoutParams) enemyPotatoImage.getLayoutParams();
        params.width = 120;
        params.height = 120;

        enemyPotatoImage.setLayoutParams(params2);
        enemyPotatoAnimation = (AnimationDrawable) enemyPotatoImage.getBackground();

        enemyPotatoImage.post(new Runnable() {
            @Override
            public void run() {
                enemyPotatoAnimation.start();
            }
        });
    }

    private int calculatePower(int level, int attacklevel){
        int power = (level-1)*2 + (attacklevel-1)*5 + 20;
        return power;
    }

    private void battleWin() {
        battlesong.stop();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        BattleWin win = new BattleWin();
        transaction.add(R.id.battle_layout, win);
        transaction.commit();
    }

    private void battleLose() {
        battlesong.stop();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        BattleLoss loss = new BattleLoss();
        transaction.add(R.id.battle_layout, loss);
        transaction.commit();
    }

    public void showAlertDialog(String title, String message, int drawable_icon) {
        final String alert_title = title;
        final String alert_message = message;
        final int alert_icon = drawable_icon;
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new AlertDialog.Builder(BattleActivity.this)
                        .setTitle(alert_title)
                        .setMessage(alert_message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(alert_icon)
                        .show();
            }
        });

    }


    public void updateHealth(int element1, int element2) {
        final int elementOne = element1;
        final int elementTwo = element2;
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                compareElements(elementOne, elementTwo);
            }
        });

    }
}
